﻿using Microsoft.AspNetCore.Mvc;
using MyFirstMVCApp.Models;

namespace MyFirstMVCApp.Controllers
{
    public class HelloWorldController : Controller
    {

        private static List<DogViewModel> dogs = new List<DogViewModel>();

        public IActionResult Index()
        {
            return View(dogs);
        }

        public string Hello()
        {
            return ("Hello!");
        }

        public string HelloMessage()
        {
            return ("Hello Message!");
        }

        public IActionResult Create()
        {
            var dogVn = new DogViewModel();
            return View(dogVn);
        }

        public IActionResult CreateDog(DogViewModel dogViewModel)
        {
            dogs.Add(dogViewModel);
            return RedirectToAction(nameof(Index));
        }
    }
}
